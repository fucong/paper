
\documentclass[10pt,conference]{IEEEtran}
\usepackage{blindtext, graphicx}
\usepackage{listings}
\usepackage{fancyhdr}
\usepackage{amsmath} 
\usepackage{amssymb}
\usepackage[english]{babel}
\usepackage[linesnumbered,boxed,commentsnumbered,ruled]{algorithm2e}
\usepackage{algorithmicx}
\usepackage{algpseudocode}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\ifCLASSINFOpdf

\else

\fi



\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{A CPU-GPU Data Transfer Optimization Approach Based On Code Migration and Merging}
%
%
% author names and IEEE memberships
% note positions of commas and nonbreaking spaces ( ~ ) LaTeX will not break
% a structure at a ~ so this keeps an author's name from being broken across
% two lines.
% use \thanks{} to gain access to the first footnote area
% a separate \thanks must be used for each paragraph as LaTeX2e's \thanks
% was not built to handle multiple paragraphs
%

\author{Yanlong Zhai,
        Cong Fu}
        %and~Jane~Doe,~\IEEEmembership{Life~Fellow,~IEEE}% <-this % stops a space

% note the % following the last \IEEEmembership and also \thanks -
% these prevent an unwanted space from occurring between the last author name
% and the end of the author line. i.e., if you had this:
%
% \author{....lastname \thanks{...} \thanks{...} }
%                     ^------------^------------^----Do not want these spaces!
%
% a space would be appended to the last name and could cause every name on that
% line to be shifted left slightly. This is one of those "LaTeX things". For
% instance, "\textbf{A} \textbf{B}" will typeset as "A B" not "AB". To get
% "AB" then you have to do: "\textbf{A}\textbf{B}"
% \thanks is no different in this regard, so shield the last } of each \thanks
% that ends a line with a % and do not let a space in before the next \thanks.
% Spaces after \IEEEmembership other than the last one are OK (and needed) as
% you are supposed to have spaces between the names. For what it is worth,
% this is a minor point as most people would not even notice if the said evil
% space somehow managed to creep in.



% The paper headers

% The only time the second header will appear is for the odd numbered pages
% after the title page when using the twoside option.
%
% *** Note that you probably will NOT want to include the author's ***
% *** name in the headers of peer review papers.                   ***
% You can use \ifCLASSOPTIONpeerreview for conditional compilation here if
% you desire.




% If you want to put a publisher's ID mark on the page you can do it like
% this:
%\IEEEpubid{0000--0000/00\$00.00~\copyright~2007 IEEE}
% Remember, if you use this you must call \IEEEpubidadjcol in the second
% column for its text to clear the IEEEpubid mark.



% use for special paper notices
%\IEEEspecialpapernotice{(Invited Paper)}




% make the title area
\maketitle


\begin{abstract}
%\boldmath
Recent development in graphic processing units (GPUs) has subverted the traditional CPU-only computing. Because of the high performance and parallelism, GPUs are widely used as coprocessors to run highly parallel computations. However, porting applications to CPU-GPU architecture remains a challenge to average programmers, which have to explicitly manage data transfers between the host and device memories, and manually optimize GPU memory utilization. In this paper, we proposed an approach to optimize the data transfer operations between CPU and GPU by analyzing the data dependency and reorganizing source code. Based on our previous work, we found that not only the data transmission through PCIe bus is time consuming, but also the preparation and cleaning work for data transfer operations. This cost will be dramatically increased if the program contains many kernel calls and sometimes the situation is getting severe when programmers are not sophisticated enough to make GPU programs. Therefore, we firstly defined and analyzed the data copy in (out) path for each data transfer operation utilizing compiler techniques. The data copy 
0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.in or copy out operation can be migrated along with its data copy path. Multiple data transfer operations could be merged into one operation if they are of the same transfer direction and their data copy paths have overlap. Migrating and merging multiple data transfer operations could obviously reduce the number of data exchange times and the system resource consumption. The experimental results show that our approach has a significant effect on optimizing the parallel programs especially when the program has numerous kernel calls.

\end{abstract}
% IEEEtran.cls defaults to using nonbold math in the Abstract.
% This preserves the distinction between vectors and scalars. However,
% if the journal you are submitting to favors bold math in the abstract,
% then you can use LaTeX's standard command \boldmath at the very start
% of the abstract to achieve this. Many IEEE journals frown on math
% in the abstract anyway.

% Note that keywords are not normally used for peerreview papers.
\begin{IEEEkeywords}
GPU, data transfer, optimization, code migration, merging
\end{IEEEkeywords}






% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.
\IEEEpeerreviewmaketitle



\section{Introduction}
% \blindtext
Currently, even entry-level PCs are equipped with GPUs capable of hundreds of GFLOPS. Initially GPUs were only used for graphics related computing, but due to their high processing power, now these are used as coprocessor to help CPU deal with high parallel and high computing operations. This helped a lot in performance gain and speed up the processes between 4x and 100x [1, 2, 3], but writing excellent parallel code is a challenge for average programmers. For example, CUDA is a simple C-like interface proposed for programming NVIDIA GPUs, but it is difficult to port applications to CUDA which needs programmers to manage data transfer between the host and GPU memories explicitly [4]. How to manage data transfer operations to optimize parallel programs is the focus of this paper.

In recent years, researchers around the world have adopted a number of ways to optimize parallel programs. Some optimizations are based on the GPU physical properties, such as vectorization (An array is represented as a vector and modify the code logic to use the correct element of the vector) [5] to make full use of GPU cores, using read only data cache to store constants [3]. Other optimization methods are based on common compiler optimization method, such as loop invariant code motion (moving the invariant part out of loop structure) [6, 7] and loop unrolling (reducing or eliminating instructions that control the loop) [8]. Table~\ref{parallel_code_optimizations} shows some of the methods that have been applied to optimize parallel code.

\begin{table}
  \centering
  \caption{parallel code optimizations}
  \begin{tabular}{ll}
    \hline
    % aftBackus:60er \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
    Type                & Optimization      \\
    \hline
    Based on GPU      & Vectorization        \\
                      & Parallel loop swap   \\
                      & Texture fetching     \\
                      & Coalesced global memory access    \\
                      & Using read only data cache         \\
    Compiler optimization methods
                      & Loop unrolling     \\
                      &Common sub-expression elimination\\
                      &Loop invariant code motion\\
    \hline
  \end{tabular}
  \label{parallel_code_optimizations}
\end{table}

Assume that the above optimization methods have been done well enough, and GPU performance can be fully exploited now. Even so, we found that parallel programs were not as efficient as expected due to the typical CPU-GPU memory architecture. The memories of CPU and GPU are isolated, so when the program switches between different processors, it is necessary to copy the needful data into the corresponding memory [9, 10]. Data transfer between CPU and GPU is based on PCIe bus which bandwidth can reach 8Gbps [11, 12]. Despite the high bandwidth, there is also a demonstrable delay if programmers can’t manage data transfer reasonably on PCIe bus [13]. We find that not all transmission time is used for data transfer on PCIe bus because system consumption for opening and closing data transfer should not be neglected, especially in parallel code with many kernels. Frequent data transfer becomes the bottleneck of high parallel programs, so optimizing data transfer between CPU and GPU is the key to optimize parallel code.

This paper based on the CPU-GPU memory architecture, proposes a new algorithm that merging multiple data transfer operations after migrating data transfer to optimize parallel programs in complier phase. In original parallel code, each single kernel is parallel in the GPU, but from the perspective of CPU, multiple kernels are executed in serial. Data transfer operations of different kernels are not overlapping, that making parallel code work well, but not efficiently. In this paper, at first, we migrate each data transfer operation by code motion to make transfer operations could be executed in an interval of code lines, not at a fixed code line as before. That makes data transfer operations of different kernels overlap. Then, based on the algorithms proposed in this paper, data transfer operations of each kernel will be executed at a proper code line in the code lines interval. So, it is legal to make two transfer operations of different kernels executed at the same time if they are of the same transfer direction and their executing code line intervals overlap, and delay some data which is used in follow-up kernels freeing in GPU to avoid unnecessary data exchange. After merging data transfer operations, we can reduce the frequency of data exchange operations, even the total amount of data to transfer. This is the starting point of optimizing parallel programs in this paper, and we will describe the algorithms details and display the experimental results in subsequent chapters.


% \subsection{Subsection Heading Here}
% \blindtext

% needed in second column of first page if using \IEEEpubid
%\IEEEpubidadjcol

% An example of a floating figure using the graphicx package.
% Note that \label must occur AFTER (or within) \caption.
% For figures, \caption should occur after the \includegraphics.
% Note that IEEEtran v1.7 and later has special internal code that
% is designed to preserve the operation of \label within \caption
% even when the captionsoff option is in effect. However, because
% of issues like this, it may be the safest practice to put all your
% \label just after \caption rather than within \caption{}.
%
% Reminder: the "draftcls" or "draftclsnofoot", not "draft", class
% option should be used if it is desired that the figures are to be
% displayed while in draft mode.
%
%\begin{figure}[!t]
%\centering
%\includegraphics[width=2.5in]{myfigure}
% where an .eps filename suffix will be assumed under latex,
% and a .pdf suffix will be assumed for pdflatex; or what has been declared
% via \DeclareGraphicsExtensions.
%\caption{Simulation Results}
%\label{fig_sim}
%\end{figure}

% Note that IEEE typically puts floats only at the top, even when this
% results in a large percentage of a column being occupied by floats.


% An example of a double column floating figure using two subfigures.
% (The subfig.sty package must be loaded for this to work.)
% The subfigure \label commands are set within each subfloat command, the
% \label for the overall figure must come after \caption.
% \hfil must be used as a separator to get equal spacing.
% The subfigure.sty package works much the same way, except \subfigure is
% used instead of \subfloat.
%
%\begin{figure*}[!t]
%\centerline{\subfloat[Case I]\includegraphics[width=2.5in]{subfigcase1}%
%\label{fig_first_case}}
%\hfil
%\subfloat[Case II]{\includegraphics[width=2.5in]{subfigcase2}%
%\label{fig_second_case}}}
%\caption{Simulation results}
%\label{fig_sim}
%\end{figure*}
%
% Note that often IEEE papers with subfigures do not employ subfigure
% captions (using the optional argument to \subfloat), but instead will
% reference/describe all of them (a), (b), etc., within the main caption.


% An example of a floating table. Note that, for IEEE style tables, the
% \caption command should come BEFORE the table. Table text will default to
% \footnotesize as IEEE normally uses this smaller font for tables.
% The \label must come after \caption as always.
%
%\begin{table}[!t]
%% increase table row spacing, adjust to taste
%\renewcommand{\arraystretch}{1.3}
% if using array.sty, it might be a good idea to tweak the value of
% \extrarowheight as needed to properly center the text within the cells
%\caption{An Example of a Table}
%\label{table_example}
%\centering
%% Some packages, such as MDW tools, offer better commands for making tables
%% than the plain LaTeX2e tabular which is used here.
%\begin{tabular}{|c||c|}
%\hline
%One & Two\\
%\hline
%Three & Four\\
%\hline
%\end{tabular}
%\end{table}


% Note that IEEE does not put floats in the very first column - or typically
% anywhere on the first page for that matter. Also, in-text middle ("here")
% positioning is not used. Most IEEE journals use top floats exclusively.
% Note that, LaTeX2e, unlike IEEE journals, places footnotes above bottom
% floats. This can be corrected via the \fnbelowfloat command of the
% stfloats package.

\section{Migrate data transfer operation}
In the program which is processed by the CPU-GPU processor, according to the different processor executing code, we can classify them into CPU code and GPU code. Obviously CPU code is strongly logic and lower parallel, GPU as a coprocessor has the ability to quickly perform complex mathematical and geometric calculations, GPU code is high concurrency and high computing. In the case of program flow, each kernel is related two data transfers between CPU and GPU. Before kernel execution, the corresponding data was copied to GPU memory (data copyin), and then CPU waits for GPU to run kernel (execution). After execution, the GPU will copy the results back to CPU memory (data copyout) and free the data (data freeing) finally.

\subsection{Constraint of data transfer operation migrating}
A kernel execution process is divided into four separate stages: data copyin, execution, data copyout, and data freeing. Obviously, each stage cannot be reversed, a kernel must be executed in the order of data copyin -> execution -> data copyout -> data freeing. But the specific executing time can be adjustable. Data copyin operation can move up in code sequence until the code line changes the corresponding data. Data copyout operation can move down in code sequence to delay, but must make sure be done before CPU code using the corresponding data. After data copyout, GPU will free the corresponding data immediately. But, if the data will be used in follow-up kernels again, we make GPU keep the data until the follow-up kernels executed to reduce the total amount of data exchange.

\subsection{Migrating by code motion}
Now we will introduce how to adjust four stages described previously and how to migrate data transfer operations by code motion in a single kernel. As shown in Listing 1, the initial CUDA code structure describes that, data copyin, execution, data copyout, and data freeing operations are an integral whole. After migrating, as shown in Listing 2, data copyin operation is executed in advance while data copyout and data freeing operations are postponed. Obviously these data transfer operations migrating does not affect results of the program. Data copyin operation can move up along the code sequence, and it is legal to copy data to GPU memory before kernel execution and after the data changed in CPU code. Similarly data copyout operation can move down along the code sequence, and it is legal to copy out data to CPU memory after kernel execution and before the data used in CPU code. We call the conversion from the code structure in Listing 1 to the code structure in Listing2 data transfer operation migrating
%\begin{lstlisting}[language=C]
%int main(int argc, char ** argv) { printf("Hello world!\n");
%
%return 0; }
%\end{lstlisting}
%2 listings

\section{Migrating algorithms}

\subsection{Basic block descriptions}
However, CPU or GPU does not execute the code line by line along code sequence, influenced by loop and judgment logic, some code lines will be jumped for execution. It is a mistake to move data transfer operations that must be executed to code area that might not be executed. In this paper, we introduce concept of Basic Block (BB) in Fundamentals of Compiling. In figure 1a, the code structure is divided into four $BB_s$, so-called BB is a straight-line code sequence with no branches in except entry and no branches out except exit. A BB has only one entry point and only one exit point, so it must be entered from its entry and must be exited from exit. In figure 1b, $BB_I$, $BB_J$, $BB_K$, $BB_L$ and control block branch (B), merger (M) make up the code structure described in figure 1a. $BB_I$ is prior to $BB_L$, expressed as $BB_I < BB_L$. Each BB between $BB_I$ and $BB_L$ meets principle of $BB_I \le BB \le BB_L$. All branch basic blocks ($*BB_s$) are in a pair of control blocks B and M, meeting principle of $B_n < *BB < M_n$. $B_n$, $M_n$, and all $*BB_s$ which meet $B_n < *BB < M_n$ make up a branch structure $BRANCH_n$. Apparently not all *BB will be executed, in figure 1b, within both $BB_k$ and $BB_j$, there's one and only one will be executed. If migrating data transfer operation into *BB, data exchange operation between CPU and GPU might not be executed, causing program to crash. The program does not know specific execution process in compile phase, unable to determine which *BB will be accessed. So data exchange operation must be placed outside *BB in the code sequence to ensure implementation.
\subsection{Data transfer operation migrating algorithms}
Entire program structure is composed of many code lines. Code at Nth line is expressed as $LINE_N$, and whole program structure is made up of [$LINE_1, LINE_end$]. Data transfer operation migrating algorithm is to obtain a largest set of code lines (copypath) to migrate data copyin or data copyout operations. This paper optimizes parallel programs from the perspective of data transfer, so we don't need to move kernel execution in code sequence. Based on the BB, data copyin and data copyout operations are moved with code line unit. This algorithm is aimed at a single kernel, so we don't care whether the data after data copyout in GPU will be used again by subsequent kernels. After data copyout operation, GPU will free the data immediately at data freepath which is a fixed code line, not a code lines interval.
\subsubsection{Obtain data copyinpath}
Data copyin operation can move upward along code sequence until current code line or current BRANCH changes (kills) the corresponding data. $LINE_i$ does not kill dataA expressed as  kill (dataA, $LINE_i$), and each code line in $BRANCH_i$ does not kill dataA expressed as kill (dataA, $BRANCH_i$). Take moving upward dataA copyin operation as an example, initial line of dataA copyin is lower bound of upward movement. Current BB is initial BB, and initial copyinpath is empty. If current BB is not *BB, moving dataA copyin operation upward along code sequence until code line would kill dataA or exit current BB. The line begins to move and line to exit make up a moving interval, then add the code lines interval to copyinpath. If meeting exit point in current BB to exit, continue moving up and if killing dataA to exit, quit. When current BB is *BB, if all $*BB_s$ in current BRANCH do not kill dataA, moving up to exit current BRANCH and continue to generate copyinpath, otherwise quit. Final copyinpath is largest set of code lines for moving dataA copyin operation.
\begin{algorithm}
\KwIn{	copyinpath = $\emptyset$, $LINE_i$ =$ LINE_j$ = initial line of dataA copyin}

\uIf{current BB is not *BB}{
     \While{$LINE_i$ in current BB  and $\neg$kill(dataA, $LINE_i$) }
        {$LINE_i$ = $LINE_i$ - 1;}
     copyinpath = copyinpath +($LINE_i$, $LINE_j$];\

   \If {kill(dataA, $LINE_i$)}{
       end algorithm;
       }

}
\Else{
   \uIf{$\neg$ kill(dataA, current BRANCH)}{
       current BB = the BB before current BRANCH\;

       $LINE_i$ = $LINE_j$ = the last code line in current BB\;

       goto 1\;
       }
   \Else{
    end algorithm;}
}

\caption{Obtain dataA copyinpath}
\label{alg:obtain_dataA_copypath}
\end{algorithm}

\subsubsection{Obtain data copyoutpath and freepath}
Data copyout operation can move down along code sequence until current code line or current BRANCH changes or gets value of the corresponding data (use data). $LINE_i$ does not use dataA expressed as $\neg$ use (dataA, $LINE_i$), and each code line in $BRANCH_i$ does not use dataA expressed as $\neg$use (dataA, $BRANCH_i$). Algorithm~\ref{alg:obtain_dataA_copyoutpath} describes how to obtain copyoutpath and freepath of dataA.
\begin{algorithm}
\KwIn{	copyinpath = $\emptyset$, LINE$_i$ = LINE$_j$ = initial line of dataA copyout}

\uIf{current BB is not *BB}{
     \While{LINE$_j$ in current BB  and $\neg$use(dataA, LINE$_j$) }
        {LINE$_j$ = LINE$_j$ + 1;}
     copyinpath = copyinpath +[LINE$_i$, LINE$_j$);\

     freepath=Line$_j$\;

   \If {use(dataA, LINE$_j$)}{
       end algorithm;
       }

}
\Else{
   \uIf{$\neg$ use(dataA, current BRANCH)}{
       current BB = the BB after current BRANCH\;

       LINE$_i$ = LINE$_j$ = the last code line in current BB\;

       goto 1\;
       }
   \Else{
    end algorithm;}
}

\caption{Obtain dataA copyoutpath and dataA freepath}
\label{alg:obtain_dataA_copyoutpath}
\end{algorithm}

\section{Data transfer operations merging}
After migrating, it is legal to make data copyin operation and data copyout operation executed at any code line in the corresponding copypath. In a highly parallel program with multiple kernels, each kernel is no longer an independent entity, copyinpaths and copyoutpaths of different kernels will overlap. As shown in figure 3 right, dataA copyinpath overlaps dataB copyinpath, merging two data copyin operations into one. Two things happened here, first we define a new data structure dataAB which size is size of dataA plus size of dataB in CPU memory, and copy dataA and dataB to dataAB. DataAB is a contiguous area of CPU memory, so we make dataAB copyin operation replaces dataA copyin and dataB copyin. Obviously, copying dataA and dataB to dataAB requires extra time and extra space in CPU memory. Assume that the memory space is sufficient, due to high CPU memory bandwidth (Intel dual-channel DDR400 bandwidth up to 6.4GB/s [14, 15]), extra time is very small. The total amount of data exchange does not reduce after merging, but the number of times to copy data to GPU operations decreased from two to one, reducing system consumption for opening and closing data transfer operations, making the best use of transmission bandwidth.

In figure 3 left, GPU frees dataA immediately after dataA copyout operation. CPU must copy dataA into GPU again before executing kernelC. Actually, dataA copyin operation of kernelC is not necessary, because this data is same as the data which kernelA frees. In figure 4 right, dataA freeing operation of kernelA is delayed until kernelC executed. The total amount of data exchange is reduced after merging, hence speeded up program processing.
\subsection{Data transfer operations merging algorithms}
Copyinpath, copyoutpath and freepath are obtained by Algorithm~\ref{alg:obtain_dataA_copypath} and Algorithm~\ref{alg:obtain_dataA_copyoutpath}, now we will describe how to merge copypaths of different kernels optimally.
\subsubsection{Delay data freeing operation}
GPU memory is not always enough, and it's useless for GPU to keep the data which will not be used in follow-up kernels. Based on four properties (data, copyinpath, copyoutpath, freepath) in each kernel, Algorithm~\ref{alg:delay_data_freeing} presents a smart solution.
\begin{algorithm}
    \ForEach{ kernel K$_i$ in kernel set}{

     \ForEach{ kernel K$_j$ in kernel set}{

            \If{ K$_i$.data == K$_j$.data and K$_i$.freepath $\cap$ K$_j$.copyinpath $\ne \emptyset$}{
                 K$_i$.freepath    = $\emptyset$,;

                 K$_j$.copyinpath  = $\emptyset$,;
                 }
        }
        }
\caption{Delay data freeing operation}
\label{alg:delay_data_freeing}
\end{algorithm}
\subsubsection{Merge data copypaths}
This algorithm will make each data transfer operation executed at a fixed code line in corresponding data copypath. Algorithm~\ref{alg:merge_copyinpaths} proposes a greedy strategy to merge data copypaths to minimize the number of times to copy data between CPU and GPU memories. Merging copyoutpaths is the same as merging copyinpaths, so we just describe how to merge copyinpaths. Initial kernel set contains all kernels. Define an array W, W[i] is the number of times LINE$_i$ is covered by all copyinpaths in current kernel set.
\begin{algorithm}
   \While{kernel set is empty}{

    W[1,end] = {0};\

    \ForEach{ kernel K$_i$ in kernel set}{

     \ForEach{ kernel LINE$_j$ in K$_i$.copypath}
     {
        W[j]=W[j]+1;
    }
   }

    n= W.findMax();

    \ForEach{ kernel K$_i$ in kernel set}
    {

    \If{LINE$_I$ in kernel set}{

        K$_i$.copyinpath=[LINE$_n$,LINE$_n$];

         Remove K$_i$ from kernel set;
    }
    }
}
\caption{Merging copyinpaths}
\label{alg:merge_copyinpaths}
\end{algorithm}
\section{Evaluation}
Since our target is to optimize data transfer between CPU and GPU, we don't care what GPU code does and we can't reduce GPU execution time by the algorithms proposed in this paper. What we focus on is how to optimize parallel program by reducing the total amount and frequency of data exchange. So we take data transfer volume and data transfer frequency as experimental parameters, and take run-time of the programs as evaluating indicator.
\subsection{Experimental platform}
We use an Intel(R) Xeon(R) E5-2620 v2 @2.10GHZ CPU with 1.5MB of L2 cache to be the host CPU for the GPU. All GPU codes were executed on an ASPEED Graphics Family (rev 21) video card, a CUDA device with 4,799 MB of global memory. The CUDA driver version is release 5.5. The nvcc compiler release 5.5, V5.5.0 compiled all CUDA programs using default optimization level.
\subsection{Experimental results}
Our experiments are performed with and without data transfer operations merging under a set of parallel programs with different data transfer volumes and different data transfer frequencies. In order to facilitate the experiment, in each parallel program, we let the amount of data transferred per kernel is the same and we make all data copyin operations and all data copyout operations can be merged into one data copyin operation and one data copyout operation. Figure 4, figure 5 and figure 6 show the data transfer time in microseconds for parallel programs without data transfer merging in comparison to with data transfer operations merging. The three histograms show the experimental results of parallel programs with different amounts of data transferred per kernel 10KB, 20KB, 50KB and different numbers of kernels 10, 50, 100. The data transfer time of parallel program with data transfer operations merging is composed of the time to copy the data of different kernels to a new continuous CPU memory (yellow part in figure 4) and the time to transfer the continuous data to GPU memory on PCIe bus (red part). From the experimental results we can see that the data transfer time of program with data transfer merging (red part) is obviously shorter than the data transfer time of program without data transfer merging (blue part). Because the red part contains little system consumption for opening and closing data transfer operations. But as the increase of the amount of data transferred per kernel, the proportion of the time spent copying operations in CPU memory (yellow part) is greater and greater, and even makes data transfer operations merging operations have side effect on parallel program. So, the algorithms proposed in this paper have significant optimization effect on data transfer of parallel programs when the data transfer volume is relatively small and data exchange is frequent. Table 2 shows more experimental results.
\section{Related Work}
Although there has been prior work on optimizing parallel computing for CPU-GPU architecture, these implementations have not addressed how to divide and merge data transfer operations of different kernels between CPU and GPU memories. Many scholars, according to the memory structure and multi-core characteristic of GPU have proposed some optimization methods to optimize parallel computing on CPU-GPU architecture.

First we can optimize parallel programs to make full use of GPU computing resources and GPU memory resources through manual code analysis and modifications. Vectorization can be applied by using vector data types provided in CUDA as data structure and modify the code logic to use the correct element of the vector in other expressions [4, 16]. Vectorization can make full use of multi-core characteristic of GPU and open more threads to run parallel computing. Texture memory can be used in kernels by invoking texture intrinsic provided in CUDA. It needs to be bound explicitly to CUDA array allocated in device memory. Rational use of texture memory can significantly optimize parallel programs.

Then, there are some optimizations on parallel programs in the compilation phase. Loop Collapsing is performed by merging double or more nested loops into a single loop which can be done by loop analysis and transformation within the compiler [4, 17]. Loop Collapsing can increase the degree of parallelism compared with initial nested loops, and GPU will open more threads to run the loop. Thread and Thread-Block Merging can be achieved by duplicating the global memory array expressions identified by the programmer with incrementing the array indices and modifying the main loop partitioning based on the thread granularity defined by the programmer [4]. And using read only data cache to store constants in GPU can speedup GPU memory access [3].

\section{Conclusion}
We propose a new method to optimize parallel computing on CPU-GPU architecture by just optimizing data transfer between CPU and GPU memories. Our algorithms consist of two parts, one is migrating each data transfer operation by code motion in parallel program, and the other is merging data transfer operations of different kernels with greedy strategy after migrating. Through a series of experimental results, our algorithms have been proved to have a significant optimization effect on parallel computing when the data transfer volume is relatively small and data exchange is frequent. In a parallel program which has more than 10 kernels and the average amount of data transfer each kernel less than 20KB, we can reduce the total data transfer time by about 20\% by the algorithms proposed in this paper.





% if have a single appendix:
%\appendix[Proof of the Zonklar Equations]
% or
%\appendix  % for no appendix heading
% do not use \section anymore after \appendix, only \section*
% is possibly needed

% use appendices with more than one appendix
% then use \section to start each appendix
% you must declare a \section before using any
% \subsection or using \label (\appendices by itself
% starts a section numbered zero.)
%


\appendices
\section{Proof of the First Zonklar Equation}
Some text for the appendix.

% use section* for acknowledgement
\section*{Acknowledgment}


The authors would like to thank...


% Can use something like this to put references on a page
% by themselves when using endfloat and the captionsoff option.
\ifCLASSOPTIONcaptionsoff
  \newpage
\fi



% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
%\IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
%\IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% BibTeX documentation can be easily obtained at:
% http://www.ctan.org/tex-archive/biblio/bibtex/contrib/doc/
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/
%\bibliographystyle{IEEEtran}
% argument is your BibTeX string definitions and bibliography database(s)
%\bibliography{IEEEabrv,../bib/paper}
%
% <OR> manually copy in the resultant .bbl file
% set second argument of \begin to the number of references
% (used to reserve space for the reference number labels box)
\begin{thebibliography}{1}

\bibitem{IEEEhowto:kopka}
H.~Kopka and P.~W. Daly, \emph{A Guide to \LaTeX}, 3rd~ed.\hskip 1em plus
  0.5em minus 0.4em\relax Harlow, England: Addison-Wesley, 1999.

\end{thebibliography}

% biography section
%
% If you have an EPS/PDF photo (graphicx package needed) extra braces are
% needed around the contents of the optional argument to biography to prevent
% the LaTeX parser from getting confused when it sees the complicated
% \includegraphics command within an optional argument. (You could create
% your own custom macro containing the \includegraphics command to make things
% simpler here.)
%\begin{biography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{mshell}}]{Michael Shell}
% or if you just want to reserve a space for a photo:

\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{picture}}]{John Doe}
\blindtext
\end{IEEEbiography}

% You can push biographies down or up by placing
% a \vfill before or after them. The appropriate
% use of \vfill depends on what kind of text is
% on the last page and whether or not the columns
% are being equalized.

%\vfill

% Can be used to pull up biographies so that the bottom of the last one
% is flush with the other column.
%\enlargethispage{-5in}



% that's all folks
\end{document}


